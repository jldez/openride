# Testing openride

Install pytest, pytest-cov and pytest-benchmark
```
pip install pytest
pip install pytest-cov
pip install pytest-benchmark
```

Then run
```
pytest --cov openride --benchmark-columns='min, max, mean, stddev' --benchmark-name='short' --benchmark-sort='name'
```